
console.log('running background script');
  
trigger_key = 71; // g key
function doKeyPress(e){
  if (e.shiftKey && e.keyCode == trigger_key){ // if e.shiftKey is not provided then script will run at all instances of typing "G"
	e.stopPropagation();
	e.preventDefault();
	// chrome.extension.sendRequest({action: 'startRecording'}); //build newurl as per viewtext URL generated earlier.
	let a = new window.webkitSpeechRecognition()
	a.onresult = function(event) {
		document.activeElement.value = event.results[0][0].transcript;
		document.activeElement.innerText = event.results[0][0].transcript;
	}

	a.onerror = function(e) {
		console.error(e);
	}

	a.start();
  }
}

if (window == top) {
  window.addEventListener('keyup', doKeyPress); //add the keyboard handler
}