chrome.extension.onRequest.addListener(function(request, sender) {
	if(request.action === 'startRecording') {
		console.log('got request');
    
    navigator.webkitGetUserMedia({
        audio: true,
    }, function(stream) {
        stream.stop();
        
        let a = new window.webkitSpeechRecognition()
        a.onresult = function(event) {
          console.log(event.results[0][0].transcript);
        }

        a.onerror = function(e) {
          console.error(e);
        }

        a.start();
    }, function(ee) {
        console.error(ee);
        // Aw. No permission (or no microphone available).
    });
	}
});
